#script qui enlève les accents d'un fichier texte
#Léo Muff avril 2020



[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")   # objet .net pour choisir le fichier 
$objForm = New-Object System.Windows.Forms.OpenFileDialog                                    
$objForm.Filter = "text files (*.txt)|*.txt|All files (*.*)|*.*";           #options de l'objet  
$objForm.InitialDirectory = $HOME
$objForm.Title = "Selectionner un fichier :"
$objForm.FilterIndex = 1
$Show = $objForm.ShowDialog()
 
If ($Show -eq "Cancel")                                                     # si l'utilisateur annule l'action, la fenetre se ferme
{                                                                           
"Annulé par l'utilisateur"
}
Else                                                                        # si il choisit un fichier, le script s'execute
{
    $dicoAccents = [ordered]@{                                              # array  des accents et leurs correspondances                                        
        "é"="e";
        "Ã©"="e";
        "è"="e";
        "Ã¨"="e";
        "ê"="e";
        "Ãª"="e";
        "ç"="c";
        "Ã§"="c";
        "à"="a";
        "Ã "="a";
        "â"="a";
        "Ã¢"="a";
        "ô"="o";
        "Ã´"="o";
        "î"="i";
        "Ã®"="i";
        "ï"="i";
        "Ã¯"="i";
    }
    $text = Get-Content -Path $objForm.FileName                             #enrengistre le texte du fichier dans une var
    forEach ($key in $dicoAccents.Keys){
        $text = $text -replace $key, $dicoAccents[$key]                     #remplace  chaque lettre accentuée dans le texte
    }
    Clear-Host
    Write-Host $text
    $fileObject = Get-item -path $objForm.Filename                          # stocke l'objet fichier dans une var
    $path = $fileObject.BaseName+"_sans_accents"+$fileObject.Extension      # ajoute le texte "sans_accents" au nom du fichier
    Set-Content -Path $path -Value $text                                    # enrengistre le resultat dans un nouveau fichier
    
}