## accents.ps1
### Script pour enlever les accents d'un fichier texte.

Le script vous fait sélectionner un fichier et créer un nouveau une copie sans les signes accentués (fichier_sans_accents.txt)

**Voir accents_cmd.ps1 pour une utilisation en ligne de commande**


### Fonctionnement du script


1. Un objet .Net est créé pour la séléction du fichier
2. Un dictionaire comprenant les caractères à changer est créé
3. Le texte du fichier séléctionné par l'utilisateur est stocké dans une variable
4. Une boucle remplace les charactères du dictionnaire dans le texte de la variable.
5. Le texte modifié est copié dans un nouveau fichier comprenant "sans_accents" dans son nom


---
